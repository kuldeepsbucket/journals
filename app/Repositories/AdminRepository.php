<?php
namespace App\Repositories;
use App\User;
use App\Http\Models\Pages;
use Hash;
use Str;

Class AdminRepository extends BaseRepository
{
	public function __construct(){

	}

	// public function validate_admin_login($data){
	// 	$user = User::where('email',$data['admin_username'])->where('is_admin',1)->first();
	// 	if(!empty($user) && Hash::check($data['admin_pass'], $user["password"])){
	// 		return $user;
	// 	}else{
	// 		return false;
	// 	}
	// }

	public function adminUsers(){
		$users = User::where('is_admin',2)->orderBy('id','DESC')->paginate(10);
		return $users;
	}

	public function createUsers($data){
		return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_admin' => 2,
        ]);
	}

	public function deleteUser($userId){
		return User::where('id',$userId)->delete();
	}

	public function createPages($data){
		return Pages::create([
            'pg_slug' => Str::slug($data['tittle']),
            'pg_tittle' => $data['tittle'],
            'pg_content' => $data['content'],
        ]);
	}

	public function updatePages($postParams,$where){
		return Pages::where($where)->update([
			'pg_slug' => Str::slug($postParams['tittle']),
            'pg_tittle' => $postParams['tittle'],
            'pg_content' => $postParams['content'],
        ]);
	}
}
