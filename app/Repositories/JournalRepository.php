<?php
namespace App\Repositories;

use App\Http\Models\Journal;
use App\Http\Models\Volumes;
use App\Http\Models\Paper;
use App\Http\Models\Membership;
use App\Http\Models\MembersType;
use App\Http\Models\Pages;
use Auth;

Class JournalRepository extends BaseRepository
{
	public function __construct(Journal $journal, Volumes $volume,Paper $paper,Membership $membership,MembersType $membersType,Pages $pages){
		$this->journal = $journal;
		$this->volume = $volume;
		$this->paper = $paper;
		$this->membership = $membership;
		$this->membersType = $membersType;
		$this->pages = $pages;
	}

	private function removeToken($params){
		unset($params["_token"]);
		return $params;
	}

	private function processData($pre,$param,$model){
		foreach ($param as $key => $value) {
			$model[$pre.$key] = $value;
		}
		return $model;
	}

	public function createJournal($postParams){
		$inputParams = $this->removeToken($postParams);
		$data = $this->processData('jr_',$inputParams,$this->journal);
		if($data->save()){
			return true;
		}else{
			return false;
		}	
	}

	public function updateJournal($postParams,$where){
		$inputParams = $this->removeToken($postParams);
		foreach ($inputParams as $key => $value) {
			$data['jr_'.$key] = $value;
		}

		if($this->journal->where($where)->update($data)){
			return true;
		}else{
			return false;
		}	
	}

	public function createVolumes($postParams){
		$inputParams = $this->removeToken($postParams);
		$data = $this->processData('vol_',$inputParams,$this->volume);
		$data['jr_id'] = Request()->segment(2);
		if($data->save()){
			return true;
		}else{
			return false;
		}	
	}

	public function updateVolumes($postParams,$where){
		$inputParams = $this->removeToken($postParams);
		foreach ($inputParams as $key => $value) {
			$data['vol_'.$key] = $value;
		}
		
		if($this->volume->where($where)->update($data)){
			return true;
		}else{
			return false;
		}	
	}

	public function submitPaper($postParams){
		$inputParams = $this->removeToken($postParams);
		$data = $this->processData('pr_',$inputParams,$this->paper);
		$data['vol_id'] = Request()->segment(3);
		if($data->save()){
			return true;
		}else{
			return false;
		}
	}

	public function updatePaper($postParam,$where){
		return $this->paper->where($where)->update($postParam);
	}

	public function addMembership($postParams){
		$inputParams = $this->removeToken($postParams);
		$data['u_id'] = Auth::user()->id;
		$data['m_type'] = $inputParams["mt_type"];
		$data['m_detail'] = json_encode($inputParams);
		
		if (Auth::user()->is_member) {
            $this->membership->where('u_id',$data['u_id'])->update($data);
            return true;
        }else{
            if($this->membership->insert($data)){
				Auth::user()->where('id',$data['u_id'])->update(['member_type'=>$inputParams["mt_type"],'is_member' => 1]);
				return true;
			}else{
				return false;
			}
        }
		
	}

	public function getAllRecords($model){
		return $this->$model->get();
	}

	public function getOneRecords($model, $where=null){
		return $this->$model->where($where)->first();
	}

	public function getConditonalRecords($model,$conditionArray){
		return $this->$model->where($conditionArray)->get();
	}

	public function searchAuthor($postParam){
		$query = $this->paper->join('volumes','volumes.vol_id','papers.vol_id')->join('journals','volumes.jr_id','journals.jr_id');
		if ($postParam->isMethod('POST')) {
			if (!empty($postParam->input("s_author"))) {
				$query = $query->where('pr_author_name','like','%'.$postParam->input("s_author").'%');
			}
			if (!empty($postParam->input("s_title"))) {
				$query = $query->where('pr_paper_title','like','%'.$postParam->input("s_title").'%');
			}
			if (!empty($postParam->input("s_affiliation"))) {
				$query = $query->where('pr_affiliation','like','%'.$postParam->input("s_affiliation").'%');
			}
			if (!empty($postParam->input("s_location"))) {
				$query = $query->where('jr_publication_location','like','%'.$postParam->input("s_location").'%');
			}

		}
		return $authors = $query->get();
	}

	public function jounalSearch($searchParam){
		$likeQuery = "jr_name LIKE '%".$searchParam."%' OR jr_issn LIKE '%".$searchParam."%' OR jr_publisher LIKE '%".$searchParam."%'";
		return $this->journal->whereRaw($likeQuery)->get();
	}

	public function sourceSearch($searchParam){
		$query = $this->journal->with('papers');
		if ($searchParam->isMethod('POST')) {
			if (!empty($searchParam->input("SearchCol")) && !empty($searchParam->input("SearchVal"))) {
				$query = $query->where('jr_'.$searchParam->input("SearchCol"),'like','%'.$searchParam->input("SearchVal").'%');
			}
		}
		return $query->paginate(10);
	}	

}