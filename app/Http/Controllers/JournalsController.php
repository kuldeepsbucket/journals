<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\JournalRepository;
use Session;
use Redirect;
use Validator;

class JournalsController extends Controller
{

    public function __construct(JournalRepository $journalRepo){
        parent::__construct();
        $this->journalRepo = $journalRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $journalRecords = $this->journalRepo->getAllRecords('journal');
        return view('index',compact('journalRecords'));
    }

    public function journal_search(Request $request){
        $searchParam = $request->input('search_text');
        $records = $this->journalRepo->jounalSearch($searchParam);
        // echo "<pre>";print_r($records);die;
        return view('pages.journal_search',compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id = null)
    {
        $title = 'Journals';

        if ($id) {
            $journalRecords = $this->journalRepo->getOneRecords('journal',['jr_id'=>$id]);
        }else{
            $journalRecords = $this->journalRepo->getAllRecords('journal');
        }
        // echo "<pre>";print_r($journalRecords);die;
        if($request->isMethod('post')){
            $inputParam = $request->input();
            if ($request->file('logo') !== null && $request->file('logo')->isValid()) {
                $destinationPath = storage_path('journal_logo/');
                $filename = time().".".$request->file('logo')->getClientOriginalExtension();

                if($request->file('logo')->storeAs('journal_logo',$filename)){
                    $inputParam["logo"]=$filename;
                }                
            }

            if ($id) {
                $status = $this->journalRepo->updateJournal($inputParam,['jr_id'=>$id]);
            }else{
                $status = $this->journalRepo->createJournal($inputParam);                
            }

            if($status){
                Session::flash('msg','<div class="alert alert-success">Journal is created. </div>');
            }else{
                Session::flash('msg','<div class="alert alert-danger">Journal could not be saved, Please try after sometime. </div>');
            }
            return Redirect::back();
        }
        return view('pages.create_journal', compact("journalRecords","title"));
    }

    public function validateForm(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'issn' => 'required',
            'wrl' => 'required',
            'year_start' => 'required',
            'indexing' => 'required',
            'email' => 'required',
            // 'email' => 'required|unique:journals,jr_email',
            'Publisher' => 'required',
            'publication_freq' => 'required',
            'publication_location' => 'required',
            'contact_no' => 'required|max:10|min:10'
            // 'file' => 'required|max:10000|mimes:doc,docx'
        ]);

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);

    }

    public function source_search(Request $request){
        $journals = $this->journalRepo->sourceSearch($request);
        // echo "<pre>";print_r($journals);die;
        return view('pages.source',compact('journals','request'));
    }

}
