<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JournalRepository;
use Session;
use Redirect;
use Validator;

class VolumesController extends Controller
{

	public function __construct(JournalRepository $journalRepo){
        parent::__construct();
        $this->journalRepo = $journalRepo;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Request()->segment(1) == "volume-create") {
            $title = "Create Volume";
            $journalId = Request()->segment(2);
            $volumes = $this->journalRepo->getConditonalRecords('volume',["jr_id"=>$journalId]);
        }

        if (Request()->segment(1) == "volume-edit") {
            $title = "Edit Volume";
            $volumeId = Request()->segment(2);
            $volumes = $this->journalRepo->getOneRecords('volume',["vol_id"=>$volumeId]);
        }

        // echo "<pre>";print_r($volumes);die;
        if($request->isMethod('post')){
            $inputParam = $request->input();
            $inputParam['published_date'] = strtotime($request->input('published_date'));

            if (Request()->segment(1) == "volume-create"){
                $status = $this->journalRepo->createVolumes($inputParam);
            }

            if (Request()->segment(1) == "volume-edit"){
                $status = $this->journalRepo->updateVolumes($inputParam,['vol_id'=>$volumeId]);
            }

            if($status){
                Session::flash('msg','<div class="alert alert-success">Journal is created. </div>');
            }else{
                Session::flash('msg','<div class="alert alert-error">Journal could not be saved, Please try after sometime. </div>');
            }
            return Redirect::back();
        }
        return view('pages.create_volume',compact('volumes','title'));
    }

    public function validateForm(Request $request){
        // echo "<pre>";print_r($request->input());
        // $validator = Validator::make($request->all(), [
        //     'number' => 'required | unique:volumes,vol_number,'.$request->input("issue_no").',vol_issue_no',
        //     'issue_no' => 'required | unique_with:volumes,number',
        //     'published_date' => 'required',
        // ]);

        $validator = Validator::make($request->all(), [
            'number' => 'required',
            'issue_no' => 'required',
            'published_date' => 'required',
        ]);

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }
}
