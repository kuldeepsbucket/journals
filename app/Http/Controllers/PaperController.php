<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JournalRepository;
use Session;
use Redirect;


class PaperController extends Controller
{
    public function __construct(JournalRepository $journalRepo){
        parent::__construct();
    	$this->journalRepo = $journalRepo;
    }


    public function create(Request $request){
        $title = "Author";
        $authors = $this->journalRepo->getConditonalRecords('paper',["vol_id"=>$request->segment(3)]);
    	if($request->isMethod('post')){
    		$inputParam = $request->input();
    		if (!empty($request->file()) && $request->file('file_path')->isValid()) {
                $destinationPath = storage_path('journal_logo/');
                $filename = time().".".$request->file('file_path')->getClientOriginalExtension();

                if($request->file('file_path')->storeAs('papers',$filename)){
                    $inputParam["file_path"]=$filename;
                }
            }
    		$status = $this->journalRepo->submitPaper($inputParam);
    		if($status){
                Session::flash('msg','<div class="alert alert-success">Journal is created. </div>');
            }else{
                Session::flash('msg','<div class="alert alert-error">Journal could not be saved, Please try after sometime. </div>');
            }
            return Redirect::back();
    	}
    	return view('pages.author_detail',compact('authors','title'));
    }

    public function author_search(Request $request){
        $authors = $this->journalRepo->searchAuthor($request);
        return view('pages.author',compact('authors','request'));

    }

    public function publish(Request $request){
        $postParam = $request->all();
        $paperId = $request->input('paperId');
        unset($postParam['paperId']);
        $response = $this->journalRepo->updatePaper($postParam,['pr_id'=>$paperId]);
        return $response;
    }
}
