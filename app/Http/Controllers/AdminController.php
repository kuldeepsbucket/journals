<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\AdminRepository;
use App\Repositories\JournalRepository;
use Illuminate\Http\Request;
use App\Http\Models\PageContents;
use App\Http\Models\Pages;
use App\User;

use Session;
use Validator;
use Redirect;
use Auth;
use Str;
use DB;

class AdminController extends Controller
{

	public function __construct(AdminRepository $adminRepo,JournalRepository $journalRepo){
		parent::__construct();
		$this->adminRepo = $adminRepo;
		$this->journalRepo = $journalRepo;
	}

	public function index(Request $request){
		if($request->isMethod('POST')){
				$validator = Validator::make($request->all(),[
	            'admin_username' => 'required | email',
	            'admin_pass' => 'required '
	        ]);

	        if ($validator->passes()) {
	        	if(Auth::attempt(['email' => $request->input('admin_username'), 'password' => $request->input('admin_pass'), 'is_admin' => 1])){
	         		return Redirect('/admin-dashboard');
	         	}else{
	         	    return response()->json(['error'=>['Email or Passowrd is not valid']]);
	         	}   
	        }else{
	        	return response()->json(['error'=>$validator->errors()->all()]);
	        }
		}else{
		    return view('admin.login');
		}
	}

	public function addUser(Request $request){
		$title = 'Add User';

		if($request->isMethod('POST')){
			if($this->adminRepo->createUsers($request->input())){
				Session::flash('msg','<div class="alert alert-success">User Added Successfully </div>');
	        }else{
	            Session::flash('msg','<div class="alert alert-error">Please try again. </div>');
	        }
	        return Redirect::back();						
		}

		$subAdmin = $this->adminRepo->adminUsers();
		return view('admin.add_users',compact('subAdmin','title'));
	}

	public function removeUser($userId){
		if($this->adminRepo->deleteUser($userId)){
	    	Session::flash('msg','<div class="alert alert-success">User Removed Successfully </div>');
        }else{
            Session::flash('msg','<div class="alert alert-error">Please try again. </div>');
        }
        return Redirect::back();

	}

	public function validateUser(Request $request){
		$validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required| email |unique:users,email',
            'password' => 'required | confirmed'
        ]);

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);

	}

	public function add_pages(Request $request){
		$title = "Add Pages";
		$contents = PageContents::get();
		$inputParam = $request->input();
		// echo "<pre>";print_r($request->input());die;
		if ($request->isMethod('POST')) {
			if ($request->file('logo') !== null && $request->file('logo')->isValid()) {
                $destinationPath = storage_path('logo/');
                $filename = time().".".$request->file('logo')->getClientOriginalExtension();

                if($request->file('logo')->storeAs('logo',$filename)){
                    $inputParam["logo"]=$filename;
                }             
            }
			PageContents::where('page_id',1)->update($inputParam);
			return redirect('/admin-pages');
		}
		// echo "<pre>";print_r($contents);die;
		return view('admin.add_pages',compact('title',compact('contents')));
	}

	public function create_pages(Request $request,$id = null){
		$title = "Create Pages";

		if ($id) {
            $pages = $this->journalRepo->getOneRecords('pages',['pg_id'=>$id]);
        }else{
            $pages = $this->journalRepo->getAllRecords('pages');
        }

		if($request->isMethod('POST')){
			if ($id) {
                $status = $this->adminRepo->updatePages($request->input(),['pg_id'=>$id]);
            }else{
                $status = $this->adminRepo->createPages($request->input());                
            }

			if($status){
				Session::flash('msg','<div class="alert alert-success">Page Added Successfully </div>');
	        }else{
	            Session::flash('msg','<div class="alert alert-error">Please try again. </div>');
	        }
	        return Redirect::back();						
		}

		// $pages = $this->journalRepo->getAllRecords('pages');
		return view('admin.create_pages',compact('title','pages'));
	}

	public function validate_page(Request $request){
		$validator = Validator::make($request->all(),[
            'tittle' => 'required | Max:15 ',
            // 'tittle' => 'required | unique:pages,pg_tittle| Max:15 ',
        ]);

        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
	}

    // Show the requested page
    public function show($slug)
    {
    	$page = Pages::where("pg_slug",$slug)->first();
    	return view('pages.pages', compact('page'));
    }

    public function dashboard(){
    	$title = "Dashboard";
    	$query = DB::table('users')->select(DB::Raw("count(case when member_type = 1 then 1 else null end) AS student,
    	  									count(case when member_type = 2 then 1 else null end) AS institute,
    	  									count(case when (member_type = 1 OR member_type = 2) then 1 else null end) AS total"));
    	$journal = DB::table('journals')->select(DB::Raw("YEAR(created_at) AS y, MONTH(created_at) AS m, COUNT(DISTINCT jr_id) as journals_count"))->groupBy(['y','m'])->orderBy('m')->get();
    	$users = $query->first();
    	// echo "<pre>";print_r($journal);die;
    	return view('admin.dashboard',compact('title','users','journal'));
    } 

}
