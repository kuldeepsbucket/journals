<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JournalRepository;

class AuthorController extends Controller
{
	public function __construct(JournalRepository $journalRepo){
        parent::__construct();
        $this->journalRepo = $journalRepo;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->isMethod('post')){
            $status = $this->journalRepo->createJournal($request->input());
            if($status){
                Session::flash('msg','<div class="alert alert-success">Journal is created. </div>');
            }else{
                Session::flash('msg','<div class="alert alert-error">Journal could not be saved, Please try after sometime. </div>');
            }
        }
        return view('create_journal');
    }
}
