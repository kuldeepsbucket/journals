<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JournalRepository;
use Auth;
use Redirect;

class MembershipController extends Controller
{

	public function __construct(JournalRepository $journalRepo){
        parent::__construct();
		$this->journalRepo = $journalRepo;
	}

    public function index(){
        return view('widgets.membership');
    }
    
    public function get_form(Request $request){
    	$form = $request->input('form_name');
        $loggedinUser = Auth::user();
        if(isset($loggedinUser) && $loggedinUser->is_membser){
            $memberDetails = $this->journalRepo->getOneRecords("membership");
            $loggedinUser["membershipData"] = json_decode($memberDetails['m_detail']);
        }
		return view('widgets.'.$form,compact('loggedinUser'));
    }

    public function add_member(Request $request){
        $userData = $this->journalRepo->addMembership($request->input());
        return Redirect::back();
    }

    public function user_profile(){
        $loggedinUser = Auth::user();
        $memberDetails = $this->journalRepo->getOneRecords("membership",["u_id"=>Auth::user()->id]);
        $loggedinUser["membershipData"] = json_decode($memberDetails['m_detail']);

        return view('pages.user_profile',compact('loggedinUser'));
    }
}
