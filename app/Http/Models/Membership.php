<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $table = 'membership';
	protected $primaryKey = 'm_id';
	public $timestamps = true;
}
