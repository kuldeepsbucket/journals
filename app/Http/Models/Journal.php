<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $table = 'journals';
	protected $primaryKey = 'jr_id';
	public $timestamps = true;

	public function volumes(){
		return $this->hasMany('App\Http\Models\Volumes','jr_id','jr_id');
	}

	public function papers(){
	    return $this->hasManyThrough('App\Http\Models\Paper','App\Http\Models\Volumes','vol_id','vol_id');
	}
}
