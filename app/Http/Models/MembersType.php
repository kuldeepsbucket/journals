<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MembersType extends Model
{
    protected $table = 'members_type';
	protected $primaryKey = 'mt_id';
	public $timestamps = false;
}
