<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PageContents extends Model
{
    protected $table = 'page_contents';
	protected $primaryKey = 'page_id';
	public $timestamps = false;
}
