<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    protected $table = 'papers';
	protected $primaryKey = 'pr_id';
	public $timestamps = true;
}
