<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';
	protected $primaryKey = 'pg_id';
	public $timestamps = false;

	protected $fillable = [
        'pg_slug','pg_tittle', 'pg_content',
    ];
}
