<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Volumes extends Model
{
    protected $table = 'volumes';
	protected $primaryKey = 'vol_id';
	public $timestamps = true;
}
