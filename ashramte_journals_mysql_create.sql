CREATE TABLE `journals` (
	`jr_id` int(11) NOT NULL AUTO_INCREMENT,
	`jr_name` varchar(100) NOT NULL,
	`jr_issn` varchar(255) NOT NULL,
	`jr_wrl` varchar(255) NOT NULL,
	`jr_year_start` int(5) NOT NULL,
	`jr_indexing` varchar(255) NOT NULL,
	`jr_email` varchar(100) NOT NULL,
	`jr_publisher` varchar(100) NOT NULL,
	`jr_publication_freq` varchar(255) NOT NULL,
	`jr_publication_location` varchar(100) NOT NULL,
	`jr_contact_no` varchar(255) NOT NULL,
	`jr_logo` varchar(255) NOT NULL,
	`jr_notes` TEXT,
	`is_published` BOOLEAN NOT NULL DEFAULT '0',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`jr_id`)
);

CREATE TABLE `migrations` (
	`id` int(10) NOT NULL,
	`migration` varchar(255) NOT NULL,
	`batch` int(11) NOT NULL
);

CREATE TABLE `papers` (
	`pr_id` int(11) NOT NULL AUTO_INCREMENT,
	`vol_id` int(11) NOT NULL,
	`pr_author_name` varchar(255) NOT NULL,
	`pr_paper_title` varchar(255) NOT NULL,
	`pr_from_page` int(11) NOT NULL,
	`pr_to_page` int(11) NOT NULL,
	`pr_abstract` varchar(255) NOT NULL,
	`pr_keyword` varchar(255) NOT NULL,
	`pr_references` varchar(255) NOT NULL,
	`pr_file_path` varchar(255),
	`pr_notes` TEXT,
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`pr_id`)
);

CREATE TABLE `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`email` varchar(255) NOT NULL,
	`email_verified_at` TIMESTAMP,
	`password` varchar(255) NOT NULL,
	`remember_token` varchar(100),
	`is_member` BOOLEAN NOT NULL DEFAULT '0',
	`member_type` int(11) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `volumes` (
	`vol_id` int(11) NOT NULL AUTO_INCREMENT,
	`jr_id` int(11),
	`vol_number` int(11) NOT NULL,
	`vol_issue_no` int(11) NOT NULL,
	`vol_published_date` int(11) NOT NULL,
	`vol_notes` TEXT,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`vol_id`)
);

CREATE TABLE `membership` (
	`m_id` int(11) NOT NULL AUTO_INCREMENT,
	`u_id` int(11) NOT NULL,
	`m_type` int(11) NOT NULL,
	`m_detail` TEXT NOT NULL,
	PRIMARY KEY (`m_id`)
);

CREATE TABLE `members_type` (
	`mt_id` int(11) NOT NULL AUTO_INCREMENT,
	`mt_name` varchar(200) NOT NULL,
	`mt_price` int(11) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`mt_id`)
);

ALTER TABLE `papers` ADD CONSTRAINT `papers_fk0` FOREIGN KEY (`vol_id`) REFERENCES `volumes`(`vol_id`);

ALTER TABLE `users` ADD CONSTRAINT `users_fk0` FOREIGN KEY (`member_type`) REFERENCES `members_type`(`mt_id`);

ALTER TABLE `volumes` ADD CONSTRAINT `volumes_fk0` FOREIGN KEY (`jr_id`) REFERENCES `journals`(`jr_id`);

ALTER TABLE `membership` ADD CONSTRAINT `membership_fk0` FOREIGN KEY (`u_id`) REFERENCES `users`(`id`);

ALTER TABLE `membership` ADD CONSTRAINT `membership_fk1` FOREIGN KEY (`m_type`) REFERENCES `members_type`(`mt_id`);

