<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'JournalsController@index');

Route::get('/author-details', function () {
    return view('pages.author_details');
});

//============Admin Authentication Required================
Route::get("/admin-login", "AdminController@index");
Route::post("/admin-login", "AdminController@index");
Route::get('/pages/{page}', 'AdminController@show'); 


// Add login with is_admin column from user table
Route::group(['middleware' => 'auth'], function () {
	Route::get("/admin-user", "AdminController@addUser");
	Route::get("/admin-dashboard", "AdminController@dashboard");
	Route::post("/admin-user", "AdminController@addUser");
	Route::get("/remove-user/{id}", "AdminController@removeUser");
	Route::post("/validate-admin_user", "AdminController@validateUser");


	Route::get("/admin-pages", "AdminController@add_pages");
	Route::post("/admin-pages", "AdminController@add_pages");
	Route::get("/create-pages", "AdminController@create_pages");
	Route::post("/create-pages", "AdminController@create_pages");
	Route::post('/page-edit/{id}','AdminController@create_pages');
	Route::get('/page-edit/{id}','AdminController@create_pages');
	Route::post("/validate-page", "AdminController@validate_page");

});

Route::group(['middleware' => 'auth'], function () {
	Route::get('/journals','JournalsController@index')->middleware('auth');
	Route::get('/journal-create','JournalsController@create');
	Route::post('/journal-create','JournalsController@create');
	Route::post('/journal-edit/{id}','JournalsController@create');
	Route::get('/journal-edit/{id}','JournalsController@create');
	Route::post('/validate-journal','JournalsController@validateForm');

	/****Routes for Volumes********/

	Route::get('/volume-create/{jrId}','VolumesController@create');
	Route::post('/volume-create/{jrId}','VolumesController@create');
	Route::get('/volume-edit/{volId}','VolumesController@create');
	Route::post('/volume-edit/{volId}','VolumesController@create');
	Route::post('/validate-volume','VolumesController@validateForm');

	/****Routes for Paper********/

	Route::get('/paper/{jrId}/{volId}','PaperController@create');
	Route::post('/paper/{jrId}/{volId}','PaperController@create');

	Route::post('/publish','PaperController@publish');

	Route::get('/user-profile','MembershipController@user_profile');

});


/*****Routes for Widgets********/

Route::get('/membership','MembershipController@index');
Route::get('/load-form','MembershipController@get_form');
Route::post('/post-member','MembershipController@add_member');


/******Routes for Socialite********/

Route::get('/fb-redirect', 'Socialite\SocialAuthFacebookController@redirect');
Route::get('/fb-callback', 'Socialite\SocialAuthFacebookController@callback');


/*******Paypal Routes**********/

Route::get('payment', 'PayPalController@payment')->name('payment');
Route::get('cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');

/****************Filters and Search**********************/

Route::get('search-journal','JournalsController@journal_search');
Route::get('author-search','PaperController@author_search');
Route::post('author-search','PaperController@author_search');

Route::get('sources','JournalsController@source_search');
Route::post('sources','JournalsController@source_search');



