
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="full-height">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('/resources/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/resources/css/mdb.css')}}" rel="stylesheet">

 <link href="{{asset('/resources/font/linearicon/style.css')}}" rel="stylesheet">
 <link href="{{asset('/resources/font/fill/demo-files/demo.css')}}" rel="stylesheet">

    <link href="{{asset('/resources/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('/resources/css/slick-theme.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/4.4.95/css/materialdesignicons.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style>

        html,
        body,
        header,
        .jarallax {
          min-height: 600px;
        }

        @media (max-width: 740px) {
          html,
          body,
          header,
          .jarallax {
            height: 100vh;
          }
        }

        @media (min-width: 800px) and (max-width: 850px) {
          html,
          body,
          header,
          .jarallax {
            height: 100vh;
          }
        }

        @media (min-width: 560px) and (max-width: 650px) {
          header .jarallax h1 {
            margin-bottom: .5rem !important;
          }
          header .jarallax h5 {
            margin-bottom: .5rem !important;
          }
        }

        
        @media (min-width: 660px) and (max-width: 700px) {
          header .jarallax h1 {
            margin-bottom: 1.5rem !important;
          }
          header .jarallax h5 {
            margin-bottom: 1.5rem !important;
          }
        }

        .top-nav-collapse {
            background-color: #000000 !important;
        }
        .navbar:not(.top-nav-collapse) {
            background: #252a3e !important;
        }
        @media (max-width: 768px) {
            .navbar:not(.top-nav-collapse) {
                background: #2e3038 !important;
            }
        }
        
        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #2e3038!important;
            }
        }

        footer.page-footer {
            background-color: #2e3038;
        }

    </style>
</head>
<body>

    <!--Main Navigation-->

        <!--Navbar-->
        @include('includes.header')
        <!-- Intro Section -->
        @yield('home_searchbar')
    <!--Main Navigation-->

    <!--Main Layout-->
    <main>
        @yield('page_content')
    </main>
    <!--Main Layout-->


    <!--Footer-->
    <footer class="page-footer pt-4 text-center text-md-left">
        @include('includes.footer')
    </footer>
    <!--/.Footer-->


    <!--  SCRIPTS  -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('/resources/js/jquery-3.3.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('/resources/js/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('/resources/js/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('/resources/js/mdb.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('/resources/js/slick.js')}}"></script>
    <script type="text/javascript" src="{{asset('/resources/js/common.js')}}"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script>
        new WOW().init();

        $('.article-slider').slick({
            slidesToShow: 3,
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 1500,
            speed: 2000,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
        $('.article-slider2').slick({
            slidesToShow: 4,
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 1500,
            speed: 2000,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.book-slider').slick({
            slidesToShow: 5,
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 1500,
            speed: 2000,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
      // Material Select Initialization
        $(document).ready(function () {
          $('.mdb-select').materialSelect();
        });

        $('.datepicker').pickadate({
            weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            showMonthsShort: true
        })

        $(function() {
            $('#memberChange').change(function(){
                var form_name = this.options[this.selectedIndex].getAttribute("form-name");
                $.ajax({
                    url: "{{URL::to('/load-form')}}",
                    // method:"post",
                    data:{'form_name':form_name},
                    }).done(function(d) {
                        $('#membership_form').html(d);
                        $('#submit_membership').show();
                    });
                });

            if ($('#memberChange').val()) {
                $('#memberChange').trigger("change");
            }else{
                $('#submit_membership').hide();
            }

            $(document).ready(function() {
                $('#table').DataTable({
                    // "pageLength": 1
                });
            });
        });
    </script>
</body>
</html>
