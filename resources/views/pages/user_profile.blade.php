@extends('layouts.default')

  @section('page_content')

  <section class="bg-gradient-orange pt-3 pb-3">
    <div class="container">

        <div class="row justify-content-center mt-5 pt-5 mb-5">
          <div class="col-md-8 ">
                <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12"><h1 class="title mb-4"><span>Profile Detail</span></h1></div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 form-group">
                            <h5 style="font-weight: bold;">User Name</h5>
                            <label>{{Auth::user()->name}}</label>
                          </div>
                          <div class="col-md-6 form-group">
                            <h5 style="font-weight: bold;">User Email</h5>
                            <label>{{Auth::user()->email}}</label>
                          </div>
<!--                           <div class="col-md-6 form-group">
                            <h5 style="font-weight: bold;">User Name</h5>
                            <label></label>
                          </div>
 -->                        </div>                        
                    </div>
                </div>
            </div>

        </div>

    </div>

@if(! Auth::user()->is_admin)
    <div class="container">

        <div class="row justify-content-center mt-5 pt-5 mb-5">
          <div class="col-md-8 ">
                <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12"><h1 class="title mb-4"><span>Membership Detail</span></h1></div>
                        </div>
                        <div class="row">
                        @if(isset($loggedinUser["membershipData"]) && !empty($loggedinUser["membershipData"]))
                        @foreach($loggedinUser["membershipData"] as $key=>$val)    
                          @if($key !== 'mt_type')
                          <div class="col-md-6 form-group">
                            <h5 style="font-weight: bold;">{{$key}}</h5>
                            <label>{{$val}}</label>
                          </div>
                          @endif
                        @endforeach
                        @else
                        <h5 style="font-weight: bold;">You are not a member, Please by the membership now.</h5>
                        @endif
                        </div>                        
                    </div>
                </div>
            </div>

        </div>

    </div>
@endif
@endsection
  <!--Main Layout-->
