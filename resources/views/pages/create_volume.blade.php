
@extends('layouts.admin')

@section('page_content')

<div class="main-panel">
    <div class="content-wrapper">
        <div class="p-3 bg-white">
	<div class="row">
		<div class="col-md-8"><h1 class="title mb-4"><span>@if(Request::segment(1) == 'volume-edit') Edit Volume @else Volumes @endif</span></h1></div>
    @if(Request::segment(1) !== 'volume-edit')
		<div class="col-md-4 text-right"><button type="button" class="btn btn-warning" data-toggle="collapse" data-target="#enterRecord">Create Volume</button></div>
    @endif
	<div id="enterRecord" class="collapse @if(Request::segment(1) == 'volume-edit') show @endif ">
    <div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
    <form action="@if(Request::segment(1) !== 'volume-edit') {{URL::to('/volume-create/'.Request::segment(2))}} @else {{URL::to('/volume-edit/'.Request::segment(2))}} @endif" method="post" enctype="multipart/form-data" autocomplete="off">
      @csrf
      <div class="row">
    		<div class="col-md-4">
    			<div class="mt-3">
            <label for="volume_numbr">Volume No. <span class="required">*</span></label>
            <input type="number" name="number" id="volume_number" class="form-control" @if(isset($volumes['vol_published_date'])) value="{{$volumes['vol_number']}}" @endif required/>
          </div>
    		</div>
    		<div class="col-md-4">
    			<div class="mt-3">
            <label for="pssplace_numbr">Issue No. <span class="required">*</span></label>
            <input type="number" name="issue_no" id="issue_no" class="form-control" @if(isset($volumes['vol_published_date'])) value="{{$volumes['vol_issue_no']}}" @endif required/>
          </div>
    		</div>
    		<div class="col-md-4">
    			<div class="mt-3">
            <label for="publicationDate">Date of Publication <span class="required">*</span></label>
            <input type="text" name="published_date" id="publicationDate" class="form-control datepicker" @if(isset($volumes['vol_published_date'])) value="{{date('d F, Y', $volumes['vol_published_date'])}}" @endif required/>
          </div>
    		</div>
         <div class="col-md-12">
            <label for="Notes">Notes</label>
              <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
              <textarea name="notes">@if(isset($volumes['vol_notes'])) {{$volumes['vol_notes']}} @endif</textarea>
                <script>
                        CKEDITOR.replace( 'notes' );
                </script>
          </div>
        <div class="btn-block col-md-12">
          <button type="submit" onclick="validateForm(event,'{{URL::to('/validate-volume')}}');" class="btn btn-dark"> @if(Request::segment(1) !== 'volume-edit') Create @else Update @endif Volume</button>
        </div>
    	</div>
    </form>
  </div>
  @if(Request::segment(1) !== 'volume-edit')
	<table class="table table-bordered mt-2">
  @if($volumes->count() > 0)
  <thead>
    <tr>
      <th scope="col" style="font-weight: bold;">Sr. No.</th>
      <th scope="col" style="font-weight: bold;">Volume No.</th>
      <th scope="col" style="font-weight: bold;">Issue No.</th>
      <th scope="col" style="font-weight: bold;">Volumes Notes</th>
      <th scope="col" style="font-weight: bold;">Date of Publication</th>
      <th scope="col" style="font-weight: bold;">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $key =0; ?>
    @foreach($volumes as $data)
    <tr>
      <th scope="row">{{++$key}}</th>
      <td>Volume {{$data['vol_number']}}</td>
      <td><a href="{{URL::to('/paper/'.$data['jr_id'].'/'.$data['vol_id'])}}">Issue {{$data['vol_issue_no']}}</a></td>
      <td><?php echo $data['vol_notes']; ?></td>
      <td>{{date('d F, Y',$data['vol_published_date'])}}</td>
      <td>
        <a href="{{URL::to('/volume-edit/'.$data['vol_id'])}}" class="btn-table"><span class="mdi mdi-lead-pencil"></span> Edit</a>
      </td>
    </tr>
    @endforeach
  </tbody>
@else
  <thead>
    <tr>
      <th><h3>Volumes has not been created for this Journal, Please create one.</h3></th>
    </tr>
  </thead>
@endif
  </table>
  @endif
</div>
</div>
</div>
</div>
@endsection
