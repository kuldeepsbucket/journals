@extends('layouts.default')

@section('page_content')
  <section class="pt-3 pb-3">
<div class="container-fluid">

    <div class="row justify-content-center mt-5 pt-5 mb-5">
      <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                  <div class="row">
                        <div class="col-md-8"><h1 class="title mb-4"><span>Author Profile</span> Search</h1></div>
                        <!-- <div class="col-md-4 text-right"><a href="#" class="btn btn-link"><span class="mdi mdi-pencil"></span> Edit</a></div> -->
                    </div>
                    @auth
                      @php
                        $formEnabled = '';
                      @endphp
                    @endauth
                    @guest
                      @php
                        $formEnabled = 'disabled';
                      @endphp
                    @endguest
                  <form method="post" action="author-search" autocomplete="off">
                    @csrf
                    <div class="row">
                      <div class="form-group col-md-2">
                        <input type="text" name="s_author" placeholder="Author's Name" value="{{$request->input('s_author')}}" class="form-control" {{$formEnabled}} />
                      </div>
                      <div class="form-group col-md-2">
                        <input type="text" name="s_title" placeholder="Title" value="{{$request->input('s_title')}}" class="form-control" {{$formEnabled}} />
                      </div>
                      <div class="form-group col-md-2">
                        <input type="text" name="s_affiliation" placeholder="Affiliation" value="{{$request->input('s_affiliation')}}" class="form-control" {{$formEnabled}} />
                      </div>
                      <div class="form-group col-md-3">
                        <input type="text" name="s_location" placeholder="Publication Location" value="{{$request->input('s_location')}}" class="form-control" {{$formEnabled}} />
                      </div>
<!--                       <div class="form-group col-md-3">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="remember" id="remember">
                          <label class="form-check-label" for="remember">Show exact matches only</label>
                      </div>
                       </div>-->
                      <div class="form-group col-md-2">
                        <button style="padding: 4px;" class="btn-primary" type="submit" {{$formEnabled}}>Search</button>
                      </div>
                    </div>
                  </form>
                    <div class="d-flex flex-wrap justify-content-between bg-light p-2">
                      <!-- <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="remember" id="remember">
                          <label class="form-check-label" for="remember">All</label>
                      </div> -->
                      <!-- <div class="pl-4">
                        <a href="#" class="text-secondary"><span class="mdi mdi-nfc-variant"></span> Request to merge authors</a>
                      </div> -->
                      <!-- <div class="pl-4">
                        Sort On:
                        <select>
                          <option>Document Count(high-low)</option>
                          <option>Document Count(low-high)</option>
                        </select>
                      </div> -->
                    </div>
                    <div class="table-responsive">
                      <table class="table table-bordered" id="table">
                        <thead>
                          <tr>
                            <th>Sr.No.</th>
                             <th>
                              <a href="#" class="text-dark">Author <span class="mdi mdi-arrow-down"></span></a></th>
                             <th>
                               <a href="#" class="text-dark">Title <span class="mdi mdi-arrow-down"></span></a>
                             </th>
                             <!-- <th>
                               <a href="#" class="text-dark">Documents <span class="mdi mdi-arrow-down"></span></a>
                             </th> -->
                             <th>
                               <a href="#" class="text-dark" data-toggle="popover" title="Author metrics" data-content="Scopus is adding an array of metrics for researchers. As an initial step, the h-index has been added.  The h-graph is one way of displaying and comparing the productivity and impact of published work of scholars. It should only be used together with a mix of quantitative and qualitative metrics.  More metrics will be added in the course of the year"><span class="mdi mdi-information"></span> Affiliation <span class="mdi mdi-arrow-down"></span>
                               </a>
                             </th>
                             <th>
                               <a href="#" class="text-dark"> Publication Location <span class="mdi mdi-arrow-down"></span></a>
                             </th>
<!--                              <th>
                               <a href="#" class="text-dark"> Country/Territory <span class="mdi mdi-arrow-down">
                                 
                               </span></a>
                             </th> -->
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($authors) > 0)
                          @foreach($authors as $key=>$author)
                          <tr>
                            <td>
                              <div class="form-check">
                                <!-- <input class="form-check-input" type="checkbox" name="1" id="1"> -->
                                <label class="form-check-label" for="1">{{$key+1}}</label>
                              </div>
                            </td>
                            <td>
                              <a href="#" class="text-dark">{{$author['pr_author_name']}}</a>
                              <small class="d-block text-danger">(<b>Refrences:</b> {{$author['pr_references']}})</small>
                            </td>
                            <!-- <td>425 </td> -->
                            <td>{{$author['pr_paper_title']}}</td>
                            <td>{{$author['pr_affiliation']}}</td>
                            <td>{{$author['jr_publication_location']}}</td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                    </div>
                    <!-- <ul class="pagination pagination-sm">
                      
                    </ul> -->
                    </div>                         
                        </div>
                </div>
            </div>
        </div>

    </div>

</div>
</section>
@endsection
