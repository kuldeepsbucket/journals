@extends('layouts.default')

@section('page_content')
  <section class="pt-3 pb-3">
<div class="container-fluid">

    <div class="row justify-content-center mt-5 pt-5 mb-5">
      <!-- <div class="col-md-3">
        <div class="card">
                <div class="card-body">
                  <h5>Filter refine list</h5>
                  <div class="d-flex flex-wrap justify-content-between">
                    <a href="#" class="btn btn-light btn-sm">Apply</a>
                    <a href="#" class="btn btn-link btn-sm">Clear filters</a>
                  </div>
                  <button type="button" class="btn btn-link btn-block pl-0 text-left" style="font-size: 18px" data-toggle="collapse" data-target="#displayOption">Display Option <span class="mdi mdi-chevron-down float-right"></span></button>
  <div id="displayOption" class="collapse show">
    <div class="form-check">
        <input class="form-check-input" type="checkbox" name="dj" id="dj">
        <label class="form-check-label" for="dj">Display only Open Access journals</label>
    </div>
              <fieldset class="form-check mt-3">
                <input class="form-check-input" name="group1" type="radio" id="radio1" checked="checked">
                <label class="form-check-label" for="radio1">No minimum selected</label>
              </fieldset>

              <fieldset class="form-check mt-1">
                <input class="form-check-input" name="group1" type="radio" id="radio2">
                <label class="form-check-label" for="radio2">Minimum citations</label>
              </fieldset>

              <fieldset class="form-check mt-1">
                <input class="form-check-input" name="group1" type="radio" id="radio3">
                <label class="form-check-label" for="radio3">Minimum documents</label>
              </fieldset>
          <h5 class="mt-4">Citescore highest quartile</h5>
             <div class="form-check">
                <input class="form-check-input" type="checkbox" name="pp" id="pp">
                <label class="form-check-label" for="pp">Show only titles in top 10 percent</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="qa1" id="qa1">
                <label class="form-check-label" for="qa1">1st quartile</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="qa2" id="qa2">
                <label class="form-check-label" for="qa2">2nd quartile</label>
            </div>
  </div>
  <hr/>

                    <button type="button" class="btn btn-link btn-block pl-0 text-left" style="font-size: 18px" data-toggle="collapse" data-target="#sourceType">
Source type <span class="mdi mdi-chevron-down float-right"></span></button>
  <div id="sourceType" class="collapse show">

             <div class="form-check">
                <input class="form-check-input" type="checkbox" name="pp" id="pp">
                <label class="form-check-label" for="pp">Journals</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="qa1" id="qa1">
                <label class="form-check-label" for="qa1">Book Series</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="qa2" id="qa2">
                <label class="form-check-label" for="qa2">Conference Proceedings</label>
            </div>
  </div>
  <hr/>
  <div class="d-flex flex-wrap justify-content-between">
                    <a href="#" class="btn btn-light btn-sm">Apply</a>
                    <a href="#" class="btn btn-link btn-sm">Clear filters</a>
                  </div>
                </div>
        </div>
      </div> -->
      <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                  <div class="row">
                        <div class="col-md-8"><h1 class="title mb-4"><span>Source</span> Search</h1></div>
                    </div>
                    @auth
                      @php
                        $formEnabled = '';
                      @endphp
                    @endauth
                    @guest
                      @php
                        $formEnabled = 'disabled';
                      @endphp
                    @endguest
                    <form method="post" action="{{URL::to('/sources')}}" autocomplete="off">
                      @csrf
                    <div class="row">
                      <div class="form-group col-md-3">
                        <select id="SearchCol" name="SearchCol" onchange="changeSearchPlaceholder();" class="form-control" {{$formEnabled}}>
                          <option value="name" @if($request->input('SearchCol') == 'name') selected @endif >Title</option>
                          <option value="publisher" @if($request->input('SearchCol') == 'publisher') selected @endif>Publisher</option>
                          <option value="issn" @if($request->input('SearchCol') == 'issn') selected @endif>ISSN</option>
                        </select>
                      </div>
                      <div class="form-group col-md-3">
                        <input type="text" name="SearchVal" id="SearchVal" placeholder="Enter Title" value="{{$request->input('SearchVal')}}" class="form-control" {{$formEnabled}}/>
                      </div>
                      <div class="form-group col-md-3">
                        <button type="submit" class="form-control btn-primary" {{$formEnabled}}>Search</button>
                      </div>
                    </div>
                  </form>
                    <div class="row">
                      <!-- <div class="col-md-4">
                        <h4 class="pt-1">41,154 results</h4>
                      </div>
 -->                      <!-- <div class="col-md-8 text-right"> -->
                        <!-- <a href="#" class="btn btn-link"><span class="mdi mdi-download"></span> Download Scopus Source List</a> -->

                        <!-- <a href="#" class="btn btn-link"><span class="mdi mdi-information"></span>
                        Learn more about Scopus Source List</a> -->
                      <!-- </div> -->
                      <div class="col-md-3"></div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between bg-light p-2">
                      <!-- <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="remember" id="remember">
                          <label class="form-check-label" for="remember">All</label>
                      </div> -->
                      <!-- <div class="pl-4">
                        <a href="#" class="text-secondary"><span class="mdi mdi-file-excel"></span> Export to Excel</a>
                      </div>
                      <div class="pl-4">
                        <a href="#" class="text-secondary"><span class="mdi mdi-content-save"></span> Save to Source List</a>
                      </div> -->
                      <!-- <div class="pl-4">
                        View metrics for year:
                        <select>
                          <option>2020</option>
                          <option>2019</option>
                          <option>2018</option>
                        </select>
                      </div> -->
                    </div>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Sr.No.</th>
                             <th>
                              <a href="#" class="text-dark">Source title <span class="mdi"></span></a></th>
                             <th>
                               <a href="#" class="text-dark">Publisher <span class="mdi"></span></a>
                             </th>
                             <th>
                               <a href="#" class="text-dark">ISSN <span class="mdi"></span></a>
                             </th>
                             <th>
                               <a href="#" class="text-dark"> Cite Score <span class="mdi"></span></a>
                             </th>
                             <th>
                               <a href="#" class="text-dark"> Documents <span class="mdi"></span></a>
                             </th>
<!--                              <th>
                               <a href="#" class="text-dark"> % Cited <span class="mdi"></span></a>
                             </th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php $row = (($journals->currentPage()-1) * $journals->perPage())+1;?>
                          @foreach($journals as $journal)
                          <?php $count = 0 ; ?>
                          @foreach($journal['papers'] as $papers)
                            @if(!empty($papers['pr_file_path']) && file_exists(storage_path('/app/papers/'.$papers['pr_file_path'])))
                              {{$count++}}
                            @endif
                          @endforeach
                          <tr>
                            <td>
                              <div class="form-check">
                                <!-- <input class="form-check-input" type="checkbox" name="1" id="1"> -->
                                <label class="form-check-label" for="1">{{$row++}}</label>
                              </div>
                            </td>
                            <td>
                              <a href="#" class="text-dark">{{$journal['jr_name']}}</a>
                            </td>
                            <td>{{$journal['jr_publisher']}}</td>
                            <td>{{$journal['jr_issn']}}</td>
                            <td>{{$journal['jr_citescore']}}</td>
                            <td>{{$count}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                    <ul class="pagination pagination-sm">
                      {{$journals->links()}}
                    </ul>
                    </div>
                         
                        </div>
                            
                    </form>
                    
                </div>
            </div>
        </div>

    </div>

</div>

</section>
<script type="text/javascript">
  function changeSearchPlaceholder(){
    var searchTitle = document.getElementById('SearchCol');
    var selectedText = searchTitle.options[searchTitle.selectedIndex].text;
    (document.getElementById('SearchVal')).setAttribute('placeholder','Enter '+selectedText);
  }
</script>
@endsection

