
@extends('layouts.admin')

@section('page_content')
<div class="main-panel">
<div class="content-wrapper">

  <div class="row">
    <div class="col-md-8">
      <h1 class="title mb-4"><span>Journals</span></h1>
    </div>
    @if(Request::segment(1) !== 'journal-edit')
    <div class="col-md-4 text-right">
      <button type="button" class="btn btn-warning" data-toggle="collapse" data-target="#addArticle">Add Journal</button>
    </div>
    @endif
  </div>

     <div id="addArticle" class="collapse mb-4 @if(Request::segment(1) == 'journal-edit') show @endif()">
      <div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
      </div>
      <form action="@if(Request::segment(1) !== 'journal-edit') {{URL::to('/journal-create')}} @else {{URL::to('/journal-edit').'/'.Request::segment(2)}} @endif" method="post" enctype="multipart/form-data">
        @csrf
         <div class="card card-default">
          <h4 class="card-title p-4">@if(Request::segment(1) !== 'journal-edit') Add @else Edit @endif() Journal</h4>
          <div class="card-body ">
         <div class="row">
              <div class="col-md-6">
                <div class="mt-3">
                    <label for="name">Journal Name<span class="required">*</span></label>
                    <input type="text" name="name" id="name" @if(isset($journalRecords['jr_name'])) value="{{$journalRecords['jr_name']}}" @endif required class="form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                    <label for="issn">ISSN<span class="required">*</span></label>
                    <input type="text" name="issn" id="issn" class="form-control" @if(isset($journalRecords['jr_issn'])) value="{{$journalRecords['jr_issn']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="url">URL<span class="required">*</span></label>
                  <input type="text" name="wrl" id="wrl" class="form-control" @if(isset($journalRecords['jr_wrl'])) value="{{$journalRecords['jr_wrl']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                   <label for="year_start">Starting Year<span class="required">*</span></label>
                  <select name="year_start" id="year_start" class="mt-3 form-control" @if(isset($journalRecords['jr_year_start'])) value="{{$journalRecords['jr_year_start']}}" @endif required>
                    <!-- <option>Select Starting Year</option> -->
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="indexing">Indexing <span class="required">*</span></label>
                  <input type="text" name="indexing" id="indexing" class="form-control" @if(isset($journalRecords['jr_indexing'])) value="{{$journalRecords['jr_indexing']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="email">Email<span class="required">*</span></label>
                  <input type="text" name="email" id="email" class="form-control" @if(isset($journalRecords['jr_email']))value="{{$journalRecords['jr_email']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="Publisher">Publisher Name<span class="required">*</span></label>
                  <input type="text" name="Publisher" id="Publisher" class="form-control" @if(isset($journalRecords['jr_publisher'])) value="{{$journalRecords['jr_publisher']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="Publisher">Frequency of publication<span class="required">*</span></label>
                  <input type="text" name="publication_freq" id="publication_freq" class="form-control" @if(isset($journalRecords['jr_publication_freq'])) value="{{$journalRecords['jr_publication_freq']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                 <div class="mt-3">
                    <label for="publication_location">Location of Publication<span class="required">*</span></label>
                    <input type="text" name="publication_location" id="publication_location" class="form-control" @if(isset($journalRecords['jr_publication_location'])) value="{{$journalRecords['jr_publication_location']}}" @endif required/>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                    <label for="contact_no">Contact Person No<span class="required">*</span></label>
                    <input type="text" id="contact_no" name="contact_no" class="form-control" @if(isset($journalRecords['jr_contact_no'])) value="{{$journalRecords['jr_contact_no']}}" @endif required/>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                    <label for="contact_no">Cite Score<span class="required"></span></label>
                    <input type="float" id="sitescore" name="citescore" class="form-control" @if(isset($journalRecords['jr_citescore'])) value="{{$journalRecords['jr_citescore']}}" @endif required/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="mt-3">
                  <label for="contact_no">Logo<span class="required">*</span></label>
                  <input type="file" name="logo" id="file" class="form-control" placeholder="Upload Logo" accept="file/*">
                </div>
              </div>
              <div class="col-md-12">
                <div class="mt-3">
                  <label for="Notes">Notes</label>
                  <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
                    <textarea name="notes">@if(isset($journalRecords['jr_notes'])) {{$journalRecords['jr_notes']}} @endif </textarea>
                    <script>
                            CKEDITOR.replace( 'notes' );
                    </script>
                  </div>
                </div>

          </div>


          <div class="btn-block mt-2">
            <button type="submit" style="float: right;" onclick="validateForm(event,'{{URL::to('/validate-journal')}}');" class="btn btn-dark">@if(Request::segment(1) !== 'journal-edit') Add @else Update @endif Journal</button>
          </div>
        </div>
      </div>

      </form>
    </div>

  @if(Request::segment(1) !== 'journal-edit')

    <div class="article-logo-grid d-flex flex-wrap">
      @if(count($journalRecords)>0)
      @foreach($journalRecords as $journals)

      <div class="article-logo-card mb-4 bg-white shadow position-relative">

        <div style="background-image: url('{{(!empty($journals['jr_logo']) && file_exists(storage_path('app/journal_logo/'.$journals['jr_logo']))) ? asset('storage/app/journal_logo/'.$journals['jr_logo']) : asset('storage/app/journal_logo/no-journal.png')}}');" class="article-thumb-small mr-auto ml-auto"></div>
      <!-- Title -->
              <h5 class="card-title font-weight-bold mb-2">{{$journals["jr_name"]}}</h5>
              <p class="card-text"><span class="lnr lnr-pencil"></span> {{$journals['jr_year_start']}}</p>
        <a href="{{URL::to('/volume-create/'.$journals['jr_id'])}}" class="btn-goto">
         <span class="mdi mdi-eye"></span>
        </a>
        <a href="{{URL::to('/journal-edit/'.$journals['jr_id'])}}" class="btn-like">
          <span class="mdi mdi-lead-pencil"></span>
        </a>
        </div>
        @endforeach
        @else
        No Record Found, Please Add a Journal.
        @endif
    </div>

  @endif()
<script type="text/javascript">
  var start = 1900;
  var end = new Date().getFullYear();
  var options = "";
  for(var year = end ; year >=start; year--){
    options += "<option>"+ year +"</option>";
  }
  document.getElementById("year_start").innerHTML += options;

</script>

</div>
</div>

@endsection
