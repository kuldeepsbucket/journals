
@extends('layouts.default')

@section('page_content')

<section class="pt-3 pb-3">
<div class="container">

    <div class="row justify-content-center mt-5 pt-5 mb-5">
    	<div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <form method="post" action="{{URL::to('/post-member')}}">

                	<div class="row">
                        <div class="col-md-6"><h1 class="title mb-4"><span>{{$page['pg_tittle']}}</span></h1></div>
                    </div>
                        <div class="row" style="margin-top: 45px;">    
                        	@if(!empty($page['pg_content']))
                        	<?php echo $page['pg_content']; ?>
                        	@else
                        	<div style="margin-left: 350px;color: red;"><h1>Comming Soon !</div></h1>
                        	@endif
                        </div>
                            
                        <div class="col-md-12">
                           <button class="btn btn-dark" id="submit_membership" type="submit">Add Details</button>
                        </div>                        
                </div>
            </div>
        </div>

    </div>

</div>

@endsection