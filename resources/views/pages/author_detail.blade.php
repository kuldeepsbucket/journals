
@extends('layouts.admin')

@section('page_content')

<div class="main-panel">
<div class="content-wrapper">
<div class="bg-white p-2">
		@include('pages.add-paper_form')
	<table class="table table-bordered table-striped mt-2">
  <tbody>
    <?php $Sr = 0; ?>
    @foreach($authors as $author)
    <tr>
      <th width="100px"><h1 class="text-warning text-center">{{++$Sr}}</h1></th>
      <td class="p-0">
        <table class="table table-bordered mb-0">
          <tr>
            <th width="20%" style="font-weight: bold;">Author's Name</th>
            <td width="50%">{{$author['pr_author_name']}}</td>
			      <td class="text-right">
              @if(Auth::user()->is_admin == 1 || Auth::user()->is_admin == 2)
              <button class="pull-left btn @if($author['is_published'] == 1) btn-success @else btn-warning @endif waves-effect waves-light btn-sm" id="publish_btn_{{$author['pr_id']}}" onclick="publish_article({{$author['pr_id']}})">@if($author['is_published'] == 1) Draft @else Publish @endif</button>
              @endif
              @if(Auth::user()->member_type == 1 || Auth::user()->is_admin == 1 || Auth::user()->is_admin == 2)
              <a target="_blank" @if(!empty($author['pr_file_path']) && file_exists(storage_path('/app/papers/'.$author['pr_file_path']))) href="{{asset('storage/app/papers/'.$author['pr_file_path'])}}" @endif class="btn-floating btn-sm btn-secondary" disable><i class="icon icon-download"></i></a>
              @endif
            </td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Paper Title</th>
            <td colspan="2">{{$author['pr_paper_title']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Page No.</th>
            <td>{{$author['pr_from_page']}}</td>
            <td>{{$author['pr_to_page']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Abstract</th>
            <td colspan="2">{{$author['pr_abstract']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Keywords</th>
            <td colspan="2">{{$author['pr_keyword']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Refrences</th>
            <td colspan="2">{{$author['pr_references']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Affiliation</th>
            <td colspan="2">{{$author['pr_affiliation']}}</td>
          </tr>
          <tr>
            <th style="font-weight: bold;">Notes</th>
            <td colspan="2"><?php echo $author['pr_notes']; ?></td>
          </tr>
        </table>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
@endsection

<script type="text/javascript">
  function publish_article(paperId){
    if($("#publish_btn_"+paperId).text() == 'Publish'){
      published_state = 1;
    }else{
      published_state = 0;
    }
    $.ajax({
        url: "{{URL::to('/publish')}}",
        type:'POST',
        data: {'is_published': published_state,'paperId': paperId},
        success: function(data) {
            if(data){
                if(published_state == 1){
                  $("#publish_btn_"+paperId).removeClass("btn-warning");
                  $("#publish_btn_"+paperId).addClass("btn-success");
                  $("#publish_btn_"+paperId).text('Draft');
                }else{
                  $("#publish_btn_"+paperId).removeClass("btn-success");
                  $("#publish_btn_"+paperId).addClass("btn-warning");
                  $("#publish_btn_"+paperId).text('Publish');
                }
            }else{
              alert("We are facing some issues, Please try in sometime");
            }
        }
    });
  }
</script>
