@extends('layouts.default')

<!-- Search bar with backgroud image-->
<div class="view jarallax jarallax-media" data-jarallax='{"speed": 0.2}' style="min-height: 250px !important;">
  <div class="mask rgba-purple-slight">
    <div class="container d-flex justify-content-center align-items-center" style="height: 122% !important;">
        <div class="row pt-5 mt-3">
            <div class="col-md-12 wow fadeIn mb-3">
                <div class="text-center">
                      <h2 class="display-4 font-weight-bold wow fadeInUp text-white">
                          Journal Name/ ISSN/ Publisher
                       </h2>
<!--                        <h5 class="mb-2 wow fadeInUp text-white" data-wow-delay="0.2s">
                            Accelerate your digital experience with our stellar services
                        </h5> -->
                        <form action="{{URL::to('/search-journal')}}" autocomplete="off">
                        @CSRF
                        <div class="input-group mb-3">
                              <input type="text" class="form-control p-4" placeholder="Search Articles" aria-label="Search Articles" name="search_text" aria-describedby="basic-addon2" value="{{isset($_GET['search_text']) ? $_GET['search_text'] : ''}}">
                              <button class="input-group-append btn btn-deep-orange m-0" id="basic-addon2" type="submit">
                                <i class="icon icon-search"></i> &nbsp; &nbsp; Search Journals
                              </button>
                            </div>
                        </form>
                    
                </div>

            </div>
        </div>
    </div>
     <!-- <a class="scroll-down section-scroll" href="#featured-list" rel="nofollow"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a> -->
  </div>
</div>
<!-- Search bar with backgroud image-->

@section('page_content')
<div class="container" id="featured-list">
    <div class="article-logo-grid d-flex flex-wrap">
    @if(count($records)>0)            
      @foreach($records as $record)
      <div class="article-logo-card mb-4 bg-white shadow position-relative">
        <div style="background-image: url('{{(!empty($record['jr_logo']) && file_exists(storage_path('app/journal_logo/'.$record['jr_logo']))) ? asset('storage/app/journal_logo/'.$record['jr_logo']) : asset('storage/app/journal_logo/no-journal.png')}}');" class="article-thumb-small mr-auto ml-auto"></div>
      <!-- Title -->
              <h5 class="card-title font-weight-bold mb-2">{{$record['jr_name']}}</h5>
              <p class="card-text"><span class="lnr lnr-pencil"></span> {{$record['jr_year_start']}}</p>
        <!-- <a href="http://journal.localhost/volume-create/1" class="btn-goto">
         <span class="mdi mdi-eye"></span>
        </a>
        <a href="http://journal.localhost/journal-edit/1" class="btn-like">
          <span class="mdi mdi-lead-pencil"></span>
        </a> -->
      </div>
      @endforeach
      @else
      <h2>No Record Found</h2>
      @endif
    </div>
</div>
@endsection