  <div class="row">
    <div class="col-md-8"><h1 class="title mb-4"><span>Author's </span> Detail</h1></div>
    <div class="col-md-4 text-right"><button type="button" class="btn btn-warning" data-toggle="collapse" data-target="#enterRecord">Enter Records</button></div>
  </div>
		
	<div id="enterRecord" class="collapse">
    <form method="post" action="{{URL::to('/paper/'.Request::segment(2).'/'.Request::segment(3))}}" enctype="multipart/form-data">
      @csrf
    	<div class="row">
    		<div class="col-md-12">
    			<div class="form-group">
            <label for="auth_name">Authr's Name <span class="required">*</span></label>
            <input type="text" name="author_name" id="author_name" class="form-control" required/>
          </div>
    		</div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="paper_title">Paper Title <span class="required">*</span></label>
            <input type="text" name="paper_title" id="paper_title" class="form-control" required/>
          </div>
        </div>
    		<div class="col-md-6">
    			<div class="form-group">
            <label for="page_name">Page No.(from) <span class="required">*</span></label>
            <input type="text" name="from_page" id="from_page" class="form-control" required/>
         </div>
    		</div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="page_name_to">Page No.(to) <span class="required">*</span></label>
            <input type="number" name="to_page" id="to_page" class="form-control" required/>
          </div>
        </div>

    		<div class="col-md-12">
			    <div class="form-group">
            <label for="abstract">Abstract</label>
            <textarea id="abstract" class="form-control md-textarea" name="abstract" rows="3"></textarea>
            <small id="abstract" class="form-text text-danger mb-4">
                only Two Words
            </small>
          </div>
    		</div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="keyword">Keyword <span class="required">*</span></label>
            <input type="text" name="keyword" id="keyword" class="form-control" required/>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="refrences">References</label>
            <textarea id="refrences" class="form-control md-textarea" name="references" rows="3"></textarea>
            <small id="refrences" class="form-text text-danger mb-4">
                100 words only
            </small>
          </div>
        </div>
        <div class="col-md-12">
            <label for="Notes">Notes</label>
              <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
              <textarea name="notes"></textarea>
                <script>
                        CKEDITOR.replace( 'notes' );
                </script>
          </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="file-field">
              <div class="btn btn-primary btn-sm float-left">
                <span>Browse Paper</span>
                <input type="file" name="file_path">
              </div>
              <!-- <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="Upload your file">
              </div> -->
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <button class="btn btn-dark" style="float: right;">Submit Paper</button>
        </div>
  	</div>
  </form>
</div>