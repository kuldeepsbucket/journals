@extends('layouts.default')

<!-- Search bar with backgroud image-->
@section('home_searchbar')
  @include('includes.home_searchbar')
@endsection
<!-- Search bar with backgroud image-->

@section('page_content')
@if(!empty(html_entity_decode($page_contents['announcement'])))
<section class="section wow fadeIn pt-4 pb-4" data-wow-delay="0.3s" id="featured-list">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-justify">
        <h3 class="title mb-4">Announcement</h3>
        {!! html_entity_decode($page_contents['announcement']) !!}
      </div>
    </div>
  </div>
</section>
@endif
    <!--Section: Features v.4-->
   <!--  <section class="section wow fadeIn article-section" data-wow-delay="0.3s">

       <div class="container pt-4 pb-4">
           <div class="row">
               <div class="col-md-3 dot-wrap">
                   <h1 class="title mb-4"><span>Latest</span> Article</h1>
                   <p>Nulla facilisi. Etiam tempus purus sed magna imperdiet</p>

                       <p class="plugin-link"><i class="lnr lnr-inbox"></i> <a href="#">Submit Your Paper</a></p>
                       <p class="plugin-link"><i class="lnr lnr-flag"></i> <a href="#">Track Your Paper</a></p>
                       <p class="plugin-link"><i class="lnr lnr-laptop-phone"></i> <a href="#">Research Data</a></p>
                       <p class="plugin-link"><i class="lnr lnr-rocket"></i> <a href="#">Article Withdrawal Policy</a></p>
                       @guest
                       <a href="{{URL::to('/membership')}}" class="btn btn-outline-danger btn-rounded waves-effect">Get Membership
                        <i class="lnr lnr-arrow-right"></i></a>
                      @endguest
               </div>
               <div class="col-md-9">
                   <div class="article-slider">
                    @foreach($journalRecords as $key=>$records)
                       <div class="single-article">
                           <div class="article-is" style="background-image: url('{{(!empty($records['jr_logo']) && file_exists(storage_path('app/journal_logo/'.$records['jr_logo']))) ? asset('storage/app/journal_logo/'.$records['jr_logo']) : asset('storage/app/journal_logo/no-journal.png')}}');">
                               <div class="article-info">
                                   <h3>{{$records["jr_name"]}}</h3>
                                   <a href="#"><i class="icon icon-pencil2"></i> Read More</a>
                               </div>
                           </div>
                       </div>
                    @endforeach
                   </div>
               </div>
           </div>
       </div>

    </section> -->
<section class="section wow fadeIn pt-4 pb-4 bg-f0efeb" data-wow-delay="0.3s">
   <div class="container">
     <div class="book-slider">
      @foreach($journalRecords as $key=>$records)
       <div class="single-book">
         <img style="height:260px;" src="{{(!empty($records['jr_logo']) && file_exists(storage_path('app/journal_logo/'.$records['jr_logo']))) ? asset('storage/app/journal_logo/'.$records['jr_logo']) : asset('storage/app/journal_logo/no-journal.png')}}" class="img-fluid">
         <div class="book-info">
            <h5>{{$records["jr_name"]}}</h5>
            <a href="#" class="feature-article-link">
                      <i class="lnr lnr-arrow-right"></i>
                    </a>
         </div>
       </div>
       @endforeach
     </div>
   </div>
</section>
<!-- <section class="pt-4 pb-4">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3 class="title mb-4"> Forum</h3>
        <div class="d-flex flex-wrap mb-2">
          <div style="background-image: url('{{asset('resources/upload/article1.JPEG')}}');" class="article-thumb-small"></div>
          <div class="forum-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="forum-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <div><a href="#">Read More</a> | <span>20</span> Answer | <span>20</span> Like | <span>20</span> View</div>
          </div>
        </div>
        <div class="d-flex flex-wrap mb-2">
          <div style="background-image: url('{{asset('resources/upload/article2.JPEG')}}');" class="article-thumb-small"></div>
          <div class="forum-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="forum-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <div><a href="#">Read More</a> | <span>20</span> Answer | <span>20</span> Like | <span>20</span> View</div>
          </div>
        </div>
        <div class="d-flex flex-wrap mb-2">
          <div style="background-image: url('{{asset('resources/upload/article3.jpg')}}');" class="article-thumb-small"></div>
          <div class="forum-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="forum-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <div><a href="#">Read More</a> | <span>20</span> Answer | <span>20</span> Like | <span>20</span> View</div>
          </div>
        </div>
         <hr class="w-100 clearfix">
         <a href="#" class="btn btn-outline-danger waves-effect position-relative">View All <i class="lnr lnr-chevron-right-circle"></i></a>
      </div>
      <div class="col-md-6">
        <h3 class="title mb-4"> Latest Updates</h3>
        <div class="d-flex flex-wrap mb-2">
          <div class="event-calendar">
            <p class="day">01</p>
            <p class="month-year">Oct, 2019</p>
          </div>
          <div class="event-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="event-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <a href="#">Read More</a>
          </div>
        </div>
        <div class="d-flex flex-wrap mb-2">
          <div class="event-calendar">
            <p class="day">20</p>
            <p class="month-year">Oct, 2019</p>
          </div>
          <div class="event-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="event-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <a href="#">Read More</a>
          </div>
        </div>
        <div class="d-flex flex-wrap mb-2">
          <div class="event-calendar">
            <p class="day">22</p>
            <p class="month-year">Oct, 2019</p>
          </div>
          <div class="event-info">
            <h5>Suspendisse molestie nunc eu facilisis sollicitudin.</h5>
            <span class="event-date"><i class="lnr lnr-calendar-full"></i> 07 Oct 2019</span>
            <a href="#">Read More</a>
          </div>
        </div>
         <hr class="w-100 clearfix">
         <a href="#" class="btn btn-outline-danger waves-effect position-relative">View All <i class="lnr lnr-chevron-right-circle"></i></a>
      </div>
    </div>
  </div>
</section> -->
@endsection
