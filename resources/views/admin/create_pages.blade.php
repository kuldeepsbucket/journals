@extends('layouts.admin')

@section('page_content')
<div class="main-panel">
<div class="content-wrapper"> 
  <div class="row">
    <div class="col-md-8"><h1 class="title mb-4"><span>@if(Request::segment(1) == 'page-edit') Edit Page @else Pages @endif</span></h1></div>
    @if(Request::segment(1) !== 'page-edit')
    <div class="col-md-4 text-right"><button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#enterRecord">Create Page</button></div>
    @endif
  <div id="enterRecord" class="collapse @if(Request::segment(1) == 'page-edit') show @endif ">
    <div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>
    <form action="@if(Request::segment(1) !== 'page-edit') {{URL::to('/create-pages')}} @else {{URL::to('/page-edit').'/'.Request::segment(2)}} @endif" method="post">
        @CSRF
        <div class="form-group">
          <label class="label">Tittle</label>
          <input type="text" name="tittle" class="form-control" @if(isset($pages['pg_tittle'])) value="{{$pages['pg_tittle']}}" @endif placeholder="Enter Page Title">
          <!-- <p class="small"><b>Permalink : </b><a class="font-italic" href="#" target="_blank">http://journals/page-name</a> -->
          <!-- <button class="btn btn-link text-dark"><i class="fa fa-pencil"></i></button></p> -->
        </div>
      <div class="form-group">
        <label class="label">Content</label>
        <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <textarea name="content">@if(isset($pages['pg_content'])) {{$pages['pg_content']}} @endif</textarea>
          <script>
                  CKEDITOR.replace( 'content' );
          </script>
      </div>
      <button type="submit" class="btn btn-primary btn-lg mt-3" onclick="validateForm(event,'{{URL::to('/validate-page')}}');">Save</button>
    </form>
  </div>
  @if(Request::segment(1) !== 'page-edit') 
  <table class="table table-bordered mt-2">
  @if($pages->count() > 0)
  <thead>
    <tr>
      <th scope="col" style="font-weight: bold;">Sr. No.</th>
      <th scope="col" style="font-weight: bold;">Page Tittle</th>
      <!-- <th scope="col" style="font-weight: bold;">Content</th> -->
      <th scope="col" style="font-weight: bold;">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $key =0; ?>
    @foreach($pages as $data)
    <tr>
      <th scope="row">{{++$key}}</th>
      <td>{{$data['pg_tittle']}}</td>
      <!-- <td><?php //echo $data['{{$data["pg_content"]}}']; ?></td> -->
      <td>
        <a href="{{URL::to('/page-edit/'.$data['pg_id'])}}" class="btn-table"><span class="mdi mdi-lead-pencil"></span> Edit</a>
      </td>
    </tr>
    @endforeach
  </tbody>
@else
  <thead>
    <tr>
      <th><h3>Pages has not been created, Please create one.</h3></th>
    </tr>  
  </thead>
@endif
  </table>
  @endif
</div>
</div>
</div>
@endsection