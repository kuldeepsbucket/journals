@extends('layouts.admin')

@section('page_content')

        <div class="main-panel">
          <div class="content-wrapper"> 
            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Pages Content</h4>
                    <div class="row">
                      <div class="col-md-12">
                        <form method="post" action="{{URL::to('admin-pages')}}" enctype="multipart/form-data">
                          <div class="form-group">
                            <label>Website Logo</label>
                            <input type="file" name="logo">
                          </div>
                        <div class="form-group">
                          <label>Paypal Button</label>
                          <textarea name="Paypal_button" rows="4" cols="50" placeholder="Paste Paypal Button Script"></textarea>
                        </div>
                        <div class="form-group">
                          <label>Announcement</label>
                          <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
                          <textarea name="announcement">@if(isset($volumes['vol_notes'])) {{$volumes['vol_notes']}} @endif</textarea>
                            <script>
                                    CKEDITOR.replace( 'announcement' );
                            </script>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg mt-3">Save</button>
                      </form>
                      </div>
                    <hr/>
                  </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
         
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
      
@endsection