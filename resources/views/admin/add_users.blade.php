@extends('layouts.admin')

@section('page_content')
<div class="main-panel">
          <div class="content-wrapper"> 
            <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add User</h4>
                    @if (Session::has('msg'))
                      {!! Session::get('msg') !!}
                    @endif
                    <div class="alert alert-danger print-error-msg" style="display:none">
                      <ul></ul>
                    </div>
                   <form method="post" action="{{URL::to('/admin-user')}}" autocomplete="off"> 
                    @CSRF
                     <div class="row">
                      <div class="col-md-2">
                        <input type="text" name="name" placeholder="Username" class="form-control">
                      </div>
                      <div class="col-md-2">
                        <input type="text" name="email" placeholder="Email ID" class="form-control">
                      </div>
                      <div class="col-md-2">
                        <input type="password" name="password" placeholder="Password" class="form-control">
                      </div>
                      <div class="col-md-2">
                        <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-primary" type="submit" onclick="validateForm(event,'{{URL::to('/validate-admin_user')}}');">Add User</button>
                      </div>
                    </div>
                   </form>
                   
                     <table class="table table-bordered mt-4">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>Email</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if(count($subAdmin)>0)
                            <?php $i = 0; ?>
                            @foreach($subAdmin as $users)
                            <tr>
                              <td>{{++$i}}
<!--                                 <div class="form-check form-check-flat">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input">1 <i class="input-helper"></i>
                                </label>
                                </div>
 -->                              </td>
                              <td>{{$users['name']}}</td>
                              <td>{{$users['email']}}</td>
                              <td>
                                <!-- <a href="#" class="btn btn-icons btn-rounded btn-outline-primary">
                                  <i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                                <!-- Button trigger modal -->
                                <a href="{{URL::to('/remove-user/'.$users['id'])}}" onclick="return confirm('Do you want to delete this user?');" ><button type="button" class="btn btn-icons btn-rounded btn-outline-danger" data-toggle="modal">
                                  <i class="fa fa-trash" aria-hidden="true"></i>
                                </button></a>
                                
                              </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                              You have not added any user.
                            </tr>
                            @endif
                          </tbody>
                          <tfoot>
                            
                          </tfoot>
                        </table>
                        {{ $subAdmin->links() }}
                  </div>
            </div>
          </div>
        </div>

        @endsection