<section class="pt-3 pb-3">
<div class="container">

    <div class="row justify-content-center mt-5 pt-5 mb-5">
    	<div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <form method="post" action="{{URL::to('/post-member')}}">

                	<div class="row">
                        <div class="col-md-6"><h1 class="title mb-4"><span>Be a</span> Member</h1></div>

                        <div class="col-md-6">
                            <select class="form-control" name="mt_type" id="memberChange" @if(isset($loggedinUser) && $loggedinUser->is_member) disabled="disabled" @endif style="background-color: #e8e1df;">
                                <option value="" disabled selected>Select A Category</option>
                                @foreach($memberTypes as $types)
                                <option form-name="{{$types->mt_name}}_membership" value="{{$types->mt_id}}" @if(isset($loggedinUser) && $loggedinUser->is_member == 1 && $loggedinUser->member_type == $types->mt_id) selected @endif>{{Ucfirst($types->mt_name)}}</option>
                                @endforeach
                            </select>
                            <!-- <label for="student_education">Select A Category <span class="required text-danger">*</span></label> -->
                            
                        </div>
                    </div>
                        @csrf
                        <div id="membership_form" class="row" style="margin-top: 45px;margin-left: 15px;margin-right: 15px;">    

                        </div>
                            
                        <div class="col-md-12">
                           <button class="btn btn-dark" id="submit_membership" type="submit">Add Details</button>
                        </div>
                        <?php echo $page_contents['paypal_button']; ?>    
                    </form>
                    
                </div>
            </div>
        </div>

    </div>

</div>
