<!------Institute Memebrship Form------->

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="student_name">Name of the Institute/University <span class="required text-danger">*</span></label>
            <input type="text" name="institute_name" id="institute_name" class="form-control" value="{{isset($loggedinUser['membershipData']->institute_name) ? $loggedinUser['membershipData']->institute_name : ''}}" required/>
        </div>
    </div>
    <div class="col-md-6">
        <label for="student_education">URL <span class="required text-danger">*</span></label>
        <input type="text" name="institute_url" id="institute_url" class="form-control" value="{{isset($loggedinUser['membershipData']->institute_url) ? $loggedinUser['membershipData']->institute_url : ''}}" required/>
    </div>
    <div class="col-md-6">
        <label for="student_education">Registration No <span class="required text-danger">*</span></label>
        <input type="text" name="registration_no" id="registration_no" class="form-control" value="{{isset($loggedinUser['membershipData']->registration_no) ? $loggedinUser['membershipData']->registration_no : ''}}" required/>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="city">Courses <span class="required text-danger">*</span></label>
            <input type="text" name="courses" value="{{isset($loggedinUser['membershipData']->courses) ? $loggedinUser['membershipData']->courses : ''}}" id="courses" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="college_name">Address<span class="required text-danger">*</span></label>
            <textarea name="institute_address" id="institute_address" class="form-control" style="height: 149px;" required > {{isset($loggedinUser['membershipData']->institute_address) ? $loggedinUser['membershipData']->institute_address : ''}} </textarea>
       </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="country">Total Students<span class="required text-danger">*</span></label>
            <input type="number" name="tot_students" value="{{isset($loggedinUser['membershipData']->tot_students) ? $loggedinUser['membershipData']->tot_students : ''}}" id="tot_students" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="email">Total Staff <span class="required text-danger">*</span></label>
            <input type="number" name="tot_staff" value="{{isset($loggedinUser['membershipData']->tot_staff) ? $loggedinUser['membershipData']->tot_staff : ''}}" id="tot_staff" class="form-control" required/>
       </div>
    </div>
    <div class="col-md-6">
        <label for="remember">Have You Taken Membership Earlier ?<span class="required text-danger">*</span></label>
        <!-- Material unchecked -->
        <div class="form-check form-check-inline">
          <input type="radio" class="form-check-input" id="materialUnchecked" name="already_member" onclick="is_checked(this)">
          <label class="form-check-label" for="materialUnchecked">Yes</label>
        </div>

        <!-- Material checked -->
        <div class="form-check form-check-inline">
          <input type="radio" class="form-check-input" id="materialChecked" name="already_member" onclick="is_checked(this)" checked>
          <label class="form-check-label" for="materialChecked">No</label>
        </div>
    </div>
    <div class="col-md-6">
        <label for="research_area">If yes then enter the Membership Id<span class="required text-danger">*</span></label>
        <input type="text" name="membership_id" id="membership_id" value="{{isset($loggedinUser['membershipData']->membership_id) ? $loggedinUser['membershipData']->membership_id : ''}}" id="membership_id" class="form-control" disabled/>
    </div>
</div>

<script type="text/javascript">
    function is_checked(ths){
        if(document.getElementById('materialUnchecked').checked){
            document.getElementById('membership_id').disabled = false;
        }else{
            document.getElementById('membership_id').disabled = true;
            document.getElementById('membership_id').required = false;
        }
    }
</script>

                      