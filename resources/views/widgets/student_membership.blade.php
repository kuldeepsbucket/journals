

<!------Student Memebrship Form------->
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
              <label for="student_name">Student's Name <span class="required text-danger">*</span></label>
              <input type="text" name="student_name" id="student_name" class="form-control" value="{{isset($loggedinUser['name']) ? $loggedinUser['name'] : ''}}" required/>
        </div>
    </div>
    <div class="col-md-6">
        <label for="student_education">Select Education <span class="required text-danger">*</span></label>
        <select class="form-control">
            <option value="" disabled selected>Education</option>
            <option value="1">Under Graduates</option>
            <option value="2">Post Graduates</option>
            <option value="3">Ph.D</option>
        </select>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="college_name">Name of the College <span class="required text-danger">*</span></label>
            <input type="text" name="college_name" value="{{isset($loggedinUser['membershipData']->college_name) ? $loggedinUser['membershipData']->college_name : ''}}" id="college_name" class="form-control" required/>
       </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="city">City <span class="required text-danger">*</span></label>
            <input type="text" name="city" value="{{isset($loggedinUser['membershipData']->city) ? $loggedinUser['membershipData']->city : ''}}" id="city" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="country">Country<span class="required text-danger">*</span></label>
            <input type="text" name="country" value="{{isset($loggedinUser['membershipData']->country) ? $loggedinUser['membershipData']->country : ''}}" id="country" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="email">Email <span class="required text-danger">*</span></label>
            <input type="text" name="email" value="{{isset($loggedinUser['email']) ? $loggedinUser['email'] : ''}}" id="email" class="form-control" required/>
       </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="research_area">Mobile<span class="required text-danger">*</span></label>
            <input type="text" name="student_mobile" value="{{isset($loggedinUser['membershipData']->student_mobile) ? $loggedinUser['membershipData']->student_mobile : ''}}" id="student_mobile" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="research_area">Address<span class="required text-danger">*</span></label>
            <textarea name="student_address" id="student_address" class="form-control" style="height: 149px;" required>{{isset($loggedinUser['membershipData']->student_address) ? $loggedinUser['membershipData']->student_address : ''}}</textarea>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="research_area">Research Area<span class="required text-danger">*</span></label>
            <input type="text" value="{{isset($loggedinUser['membershipData']->research_area) ? $loggedinUser['membershipData']->research_area : ''}}" name="research_area" id="research_area" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="refrenceID">Refrence ID <span class="required text-danger">*</span></label>
            <input type="text" name="refrenceID" value="{{isset($loggedinUser['membershipData']->refrenceID) ? $loggedinUser['membershipData']->refrenceID : ''}}" id="refrenceID" class="form-control" required/>
        </div>
    </div>
    <div class="col-md-6">
        <label for="member_type">Select Member's Type <span class="required text-danger">*</span></label>
        <select class="form-control" id="memberChange">
            <option value="" disabled selected>Type of Member for Applied</option>
            <option value="srMember" >Sr. Member</option>
            <option value="postMember">Post Member</option>
            <option value="lifeMember">Life Time Member</option>
        </select>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="member_photo">Upload Photo</label>
            <input type="file" name="photo" id="file" class="form-control" placeholder="Browse Photo" accept="file/*">
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12 ">
        <h4>Recommendation</h4>
        <hr/>
    </div>
    <div class="form-group col-md-4">
        <label class="col-md-12">Refrence Name</label>
        <input type="text" name="ref_name_1" placeholder="Refrence Name 1" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label class="col-md-12">Refrence ID</label>
        <input type="text" name="ref_id_1" placeholder="Refrence ID 1" class="form-control">
    </div>
</div>
<div class="row col-md-12">
    <div class="form-group col-md-4">
        <input type="text" name="ref_name_2" placeholder="Refrence Name 2" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <input type="text" name="ref_id_2" placeholder="Refrence ID 2" class="form-control">
    </div>
</div>
<div class="row col-md-12">
    <div class="form-group col-md-4">
        <input type="text" name="ref_name_3" placeholder="Refrence Name 3" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <input type="text" name="ref_id_3" placeholder="Refrence ID 3" class="form-control">
    </div>
</div>
