<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>C O M P A N Y N A M E</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('/resources/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/resources/css/mdb.css')}}" rel="stylesheet">

    <link href="{{asset('/resources/font/linearicon/style.css')}}" rel="stylesheet">
    <link href="{{asset('/resources/font/fill/demo-files/demo.css')}}" rel="stylesheet">

    <link href="{{asset('/resources/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('/resources/css/slick-theme.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
    <style>
      

        html,
        body,
        header,
        .jarallax {
          height: 600px;
        }

        @media (max-width: 740px) {
          html,
          body,
          header,
          .jarallax {
            height: 100vh;
          }
        }

        @media (min-width: 800px) and (max-width: 850px) {
          html,
          body,
          header,
          .jarallax {
            height: 100vh;
          }
        }

        @media (min-width: 560px) and (max-width: 650px) {
          header .jarallax h1 {
            margin-bottom: .5rem !important;
          }
          header .jarallax h5 {
            margin-bottom: .5rem !important;
          }
        }

        
        @media (min-width: 660px) and (max-width: 700px) {
          header .jarallax h1 {
            margin-bottom: 1.5rem !important;
          }
          header .jarallax h5 {
            margin-bottom: 1.5rem !important;
          }
        }

        .top-nav-collapse {
            background-color: #000000 !important;
        }
        .navbar:not(.top-nav-collapse) {
            background: #252a3e !important;
        }
        @media (max-width: 768px) {
            .navbar:not(.top-nav-collapse) {
                background: #2e3038 !important;
            }
        }
        
        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #2e3038!important;
            }
        }

        footer.page-footer {
            background-color: #2e3038;
        }

    </style>
</head>