      <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <!-- <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="profile-image">
                  <img class="img-xs rounded-circle" src="assets/images/faces/face8.jpg" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Allen Moreno</p>
                  <p class="designation">Premium user</p>
                </div>
              </a>
            </li> -->
            <li class="nav-item nav-category">Main Menu</li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="index.html">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon typcn typcn-coffee"></i>
                <span class="menu-title">Basic UI Elements</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="#">Buttons</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Dropdowns</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Typography</a>
                  </li>
                </ul>
              </div>
            </li> -->
            @if(Auth::user()->is_admin == 1)
            <li class="nav-item">
              <a class="nav-link" href="{{URL::to('/admin-dashboard')}}">
                <i class="menu-icon typcn typcn-shopping-bag"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="{{URL::to('/admin-pages')}}">
                <i class="menu-icon typcn typcn-th-large-outline"></i>
                <span class="menu-title">Page Contents</span>
              </a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="{{URL::to('/create-pages')}}">
                <i class="menu-icon typcn typcn-th-large-outline"></i>
                <span class="menu-title">Pages</span>
              </a>
            </li>
            @endif
            @if(Auth::user()->is_admin)
            <li class="nav-item">
              <a class="nav-link" href="{{URL::to('/journal-create')}}">
                <i class="menu-icon typcn typcn-th-large-outline"></i>
                <span class="menu-title">Journals</span>
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Settings</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin-user')}}">
                      <i class="menu-icon typcn typcn-shopping-bag"></i>
                      <span class="menu-title">Sub Admin</span>
                    </a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/announcement')}}">
                      <i class="menu-icon typcn typcn-shopping-bag"></i>
                      <span class="menu-title">Announcement</span>
                    </a>
                  </li> -->
                  <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin-pages')}}">
                      <i class="menu-icon typcn typcn-th-large-outline"></i>
                      <span class="menu-title">Page Contents</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </nav>