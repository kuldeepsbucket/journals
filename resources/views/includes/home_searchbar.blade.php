<div class="view jarallax jarallax-media" data-jarallax='{"speed": 0.2}'>
  <div class="mask rgba-purple-slight">
    <div class="container h-100 d-flex justify-content-center align-items-center">
        <div class="row pt-5 mt-3">
            <div class="col-md-12 wow fadeIn mb-3">
                <div class="text-center">
                      <h2 class="display-4 font-weight-bold wow fadeInUp text-white">
                          Journal Name/ ISSN/ Publisher
                       </h2>
<!--                        <h5 class="mb-2 wow fadeInUp text-white" data-wow-delay="0.2s">
                            Accelerate your digital experience with our stellar services
                        </h5> -->
                        <form action="{{URL::to('/search-journal')}}" autocomplete="off">
                        @CSRF
                        <div class="input-group mb-3">
                              <input type="text" class="form-control p-4" placeholder="Search Articles" aria-label="Search Articles" name="search_text" aria-describedby="basic-addon2" value="{{isset($_GET['search_text']) ? $_GET['search_text'] : ''}}">
                              <button class="input-group-append btn btn-deep-orange m-0" id="basic-addon2" type="submit">
                                <i class="icon icon-search"></i> &nbsp; &nbsp; Search Journals
                              </button>
                            </div>
                        </form>
                    
                </div>

            </div>
        </div>
    </div>
     <a class="scroll-down section-scroll" href="#featured-list" rel="nofollow"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
  </div>
</div>