
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{URL::to('/')}}">
                <!-- <img src="{{asset('/resources/img/logo.png')}}" width="78px;"> -->
                <img src="{{asset('storage/app/logo/'.$page_contents['logo'])}}" width="78px;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                
                <ul class="navbar-nav ml-auto">
<!--                     <li class="nav-item @if(Request::segment(1) == '') active @endif">
                        <a class="nav-link" href="{{URL::to('/')}}">Dashboard</a>
                    </li> -->
                    <li class="nav-item @if(Request::segment(1) == 'author-search') active @endif">
                        <a class="nav-link" href="{{URL::to('/author-search')}}">Author Search<span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item @if(Request::segment(1) == 'sources') active @endif">
                        <a class="nav-link" href="{{URL::to('/sources')}}">Sources<span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item @if(Request::segment(1) == 'membership') active @endif">
                        <a class="nav-link" href="{{URL::to('/membership')}}">Membership</a>
                    </li>
                    @if(count($headers) > 0)
                    @foreach($headers as $header)
                        <li class="nav-item @if(Request::segment(2) == $header['pg_slug']) active @endif">
                            <a class="nav-link" href="{{URL::to('/pages/'.$header['pg_slug'])}}">{{$header['pg_tittle']}}</a>
                        </li>
                    @endforeach
                    @endif
                    @guest
                        <li class="nav-item @if(Request::segment(1) == 'login') active @endif">
                            <a class="nav-link" href="{{ route('login') }}"><span class="mdi mdi-account-key"></span> {{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item @if(Request::segment(1) == 'register') active @endif">
                                <a class="nav-link" href="{{ route('register') }}"><span class="mdi mdi-account-edit"></span> {{ __('Register') }}</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" style="" href="{{ url('/admin-login') }}">Admin</a>
                        </li>
                    @else
<!--                         <li class="nav-item @if(Request::segment(1) == 'journal-create' || Request::segment(1) == 'volume-create' || Request::segment(1) == 'journal-edit' || Request::segment(1) == 'paper') active @endif">
                            <a class="nav-link" href="{{URL::to('/journal-create')}}">Add Journal</a>
                        </li> -->
<!--                         <li class="nav-item">
                            <a class="nav-link" href="volume-create">Add Volume</a>
                        </li>
 -->                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->is_admin)
                                <a class="dropdown-item" target="_blank" href="{{ URL::to('/admin-dashboard') }}">
                                    {{ __('Admin Dashboard') }}
                                </a>
                                @endif
                                <a class="dropdown-item" href="{{ URL::to('/user-profile') }}">
                                    {{ __('Profile') }}
                                </a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                    
                    <!-- <li class="nav-item">
                        <a class="btn btn-default" href="{{ url('/register') }}">Rgister</a>
                    </li> -->
                </ul>
                    <!-- <div class="md-search my-0">
                        <i class="icon icon-search"></i>
                    </div> -->
            </div>
        </div>
    </nav>
    <!-- Intro Section -->
    
