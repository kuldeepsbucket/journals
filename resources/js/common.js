function validateForm(ths,validationUrl){
  ths.preventDefault();
  $.ajax({
      url: validationUrl,
      type:'POST',
      data: $('form').serialize(),
      success: function(data) {
          if($.isEmptyObject(data.error)){
            // $('form').submit();
            $(ths.target).closest('form').submit();
          }else{
            printErrorMsg(data.error);
          }
      }
  });

} 


function printErrorMsg (msg) {
  $(".print-error-msg").find("ul").html('');
  $(".print-error-msg").css('display','block');
  $.each( msg, function( key, value ) {
      $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
  });
}

// function modal_content(title,body){
//   $('#exampleModal .modal-title').text(title);
//   $('#exampleModal .modal-body').text(body);
//   $('#exampleModal').modal({show:true});

  // confirm(body);
  // $(".confirm").click(function(e){
  //   // $(ths).closest('tr').remove();
  //   $('#exampleModal').modal('hide');
  //   return true
  // });
  // return false;    
// }

